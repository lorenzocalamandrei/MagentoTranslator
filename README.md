##  README.md

- author Lorenzo Calamandrei <lorenzo.calamandrei@thinkopen.it><br />
- version 0.1.0<br />
- package Translator

Script for create CSV file with translation of translate tag and translate args passed to __(string $string)

## _Use_
 
 - php translator.php install _{{API KEY of Yandex}}_
 - php translator.php {{Path where save .csv}} {{Path of magento Module}} [{{from language}}] [{{to language}}]
