<?php

/**
 * MagentoTranslator
 */

/**
 * MagentoTranslator
 *
 * This script get all translate tag and translate function and create a csv already translated
 *
 * @author Lorenzo Calamandrei <lorenzo.calamandrei@thinkopen.it>
 * @version 0.1.0
 * @package Translator
 *
 */

const absolute_Path = __DIR__;

require_once 'core/loader.php';

//dispatchEvent
(new TerminalController())->dispatch($argv);

