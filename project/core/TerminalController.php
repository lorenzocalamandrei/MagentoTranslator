<?php

/**
 * Class TerminalController
 */

/**
 * Class TerminalController
 *
 * Main Controller for decision by action in terminal
 */

class TerminalController
{
    /**
     * TerminalController constructor.
     */
    public function __construct()
    {
    }

    /**
     * dispatch
     * @param array $command
     */
    public function dispatch($command)
    {
        Debug::debug('Dispatch Event...');
        if ($command[1] == 'install') {
            $this->install($command);
            die;
        }
        if ($this->verifyInstallation()) {
            $this->run($command);
        }
        else {
            $this->install($command);
        }
    }

    /**
     * verifyInstallation
     * @return bool
     */
    private function verifyInstallation()
    {
        Debug::debug('Verify Installation');
        if (file_exists(absolute_Path . '/.config/Config.php')) {
            Debug::debug('File exist');
            require_once absolute_Path . '/.config/Config.php';
            if (Config::$apiKey != '{{KEY}}') {
                Debug::debug('Key configured');
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * install
     * @param $config
     * @throws Exception
     */
    private function install($config)
    {
        if ($this->verifyInstallation()) {
            die("Installation already done\n");
        }
        if (isset($config[1]) && $config[1] == 'install') {
            if (isset($config[2])) {
                if (!is_dir(absolute_Path . '/.config')) {
                    exec('mkdir ' . absolute_Path . '/.config');
                }
                exec('cp ' . absolute_Path . '/core/Config.php.example' . ' ' . absolute_Path . '/.config/Config.php');
                $configPhp = file_get_contents(absolute_Path . '/.config/Config.php');
                $configPhp = preg_replace("/{{KEY}}/", $config[2], $configPhp);
                file_put_contents(absolute_Path . '/.config/Config.php', $configPhp);
                require_once absolute_Path . '/.config/Config.php';
                die("Installation complete\n");
            }
            else {
                throw new Exception('The api key must be specified as php translator.php install [api_key]');
            }
        }
        else {
            throw new Exception('Must be installed the api key of Yandex first! Use php translator.php install [apy_key]');
        }
    }

    /**
     * run
     * @param array $config
     */
    private function run($config)
    {
        $textToTranslate = (new MagentoContainer($config[2]))->getTextToTranslate();
        if (isset($config[3]) && isset($config[4])) {
            (new Translator($config[3], $config[4]))->translate($textToTranslate, (new HandlerCSVMage($config[1])));
        }
        else {
            (new Translator())->translate($textToTranslate, (new HandlerCSVMage($config[1])));
        }
    }

}

