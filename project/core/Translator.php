<?php

/**
 * Class Translator
 */

/**
 * Class Translator
 *
 * Main Translator Class
 * use Yandex
 * @author Lorenzo Calamandrei <lorenzo.calamandrei@thinkopen.it>
 * @version 0.1.0
 * @package Translator
 *
 */

class Translator
{

    /** @const api string Yandex Translate */
    const api = 'https://translate.yandex.net/api/v1.5/tr/translate';

    /** @var string To language */
    private $to;

    /** @var string From language */
    private $from;

    private $curl;

    /**
    * __construct
    * Translator constructor.
    * @param string $from = 'en'
    * @param string $to = 'it'
    */
    public function __construct($from = 'en', $to = 'it')
    {
        $this->from = $from;
        $this->to = $to;
    }

    /**
     * preparecURL
     * @param string $postText
     * @return bool True = success && False = not success
     */
    private function prepare_cURL($postText)
    {
        $postData = 'text=' . $postText;
        $url = self::api . '?key=' . Config::$apiKey . '&lang=' . $this->from . '-' . $this->to;
        Debug::debug('Url called: ' . $url);
        Debug::debug('Post Data: ' . $postData);
        //cURL options
        $this->curl = curl_init();
        curl_setopt($this->curl, CURLOPT_POST, true);   //always POST data
        curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true); //always get the response of the server
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => $url,
            CURLOPT_POSTFIELDS => $postData
        ));
        return true;
    }

    /**
     * exec_cURL
     * @return string
     */
    private function exec_cURL()
    {
        Debug::debug('Starting the cURL');
        $returned = curl_exec($this->curl);
        $responseInfo = curl_getinfo($this->curl);
        Debug::debug('Response code from ' . self::api . ' : ' . $responseInfo['http_code']);
        curl_close($this->curl);
        if ($responseInfo['http_code'] == 200) {
            Debug::debug('Translated text: ' . (string)(simplexml_load_string($returned))->text);
            //$returned is a xml
            return simplexml_load_string($returned)->text;
        }
        //TODO improve control http_code for other possibilities
        return '';
    }

    /**
     * translate
     * @param array $textToTranslate
     * @param HandlerCSVMage $csvHandlerMage
     * @return bool
     * @throws Exception
     */
    public function translate($textToTranslate, HandlerCSVMage $csvHandlerMage)
    {
        Debug::debug('Starting translating...');
        if (is_array($textToTranslate)) {
            foreach ($textToTranslate as $text) {
                if (is_string($text)) {
                    Debug::debug('Translating text: ' . $text);
                    $this->prepare_cURL($text);
                    $textTranslated = $this->exec_cURL();
                    $isWritten = $csvHandlerMage->appendText(array(
                        $text,
                        $textTranslated
                    ));
                    if(!$isWritten) {
                        throw new Exception('Error on save in csv File on ' . $csvHandlerMage->pathOutput);
                    }
                }
                else {
                    return false;
                }
            }
        }
        else {
            throw new Exception('Error, array not passed');
        }
        return false;
    }

}

