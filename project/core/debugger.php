<?php

/**
 * Class Translator
 */

/**
 * Class Debugger
 *
 * Main Debugger Class
 * @author Lorenzo Calamandrei <lorenzo.calamandrei@thinkopen.it>
 * @version 0.1.0
 * @package Translator
 *
 */

class Debug
{

    /** @var bool|resource $logFile */
    public static $logFile;

    /**
     * __construct
     * Debug constructor.
     */
    public function __construct()
    {
        self::$logFile = fopen(absolute_Path . '/log/log' . uniqid() . date('dd-MM') . '.txt', 'w+');
    }

    /**
     * debug
     * @param $text
     */
    public static function debug($text)
    {
        fwrite(self::$logFile, $text . "\n");
    }

    /**
     * closeDebug
     */
    public static function closeDebug()
    {
        fclose(self::$logFile);
    }

}


