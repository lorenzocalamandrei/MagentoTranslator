<?php

/**
 * HandlerCSVMage
 */

/**
 * Class HandlerCSVMage
 *
 * Handler for csv creator after done the translations
 * @author Lorenzo Calamandrei <lorenzo.calamandrei@thinkopen.it>
 * @version 0.1.0
 * @package Translator
 *
 */

class HandlerCSVMage
{
    /** @var $pathOutput string Output file for csv contain the translation */
    public $pathOutput;

    /** @var  $streamWriter  */
    private $streamWriter;

    /**
     * __construct
     * csvHandlerMage constructor.
     * @param $path
     */
    public function __construct($path = '.')
    {
        $this->pathOutput = $path;
        //TODO control of path (if already defined a file name)
        $this->streamWriter = fopen($path . '/Translator.csv', 'a+');
    }

    /**
     * appendText
     * @param array $texts
     * @return bool
     */
    public function appendText($texts)
    {
        if (is_array($texts)) {
            $setComma = true;
            foreach ($texts as $text) {
                if (!fwrite($this->streamWriter, '"' . $text . '"' . ($setComma == true ? ', ' : "\n"))) {
                    return false;
                }
                else {
                    $setComma = !$setComma;
                }
            }
            return true;
        }
        return false;
    }

    /**
     * closeStreamer
     */
    public function closeStreamer()
    {
        fclose($this->streamWriter);
    }

}

