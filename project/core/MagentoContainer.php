<?php

/**
 * Class MagentoContainer
 */

/**
 * Class MagentoContainer
 *
 * Handler for csv creator after done the translations
 * @author Lorenzo Calamandrei <lorenzo.calamandrei@thinkopen.it>
 * @version 0.1.0
 * @package Translator
 *
 */

class MagentoContainer
{
    /** @var string $moduleDir directory of the specified Magento module folder */
    protected $moduleDir;

    /**
     * MagentoContainer constructor.
     * @param string $moduleDir
     * @throws Exception
     */
    public function __construct($moduleDir)
    {
        //control for $moduleDir like folder
        if (!is_dir($moduleDir)) {
            throw new Exception('Folder not specified');
        }
        $this->moduleDir = $moduleDir;
    }

    public function getTextToTranslate()
    {
        $textToTranslate = array();
        exec('find ' . $this->moduleDir . ' -type f -exec cat {} +', $allText);
        Debug::debug('--------------------------------------------------------');
        foreach ($allText as $line) {
            Debug::debug('On line: '. $line);
            preg_match_all("/\_\_\(\'(.*?)\'\)/", $line, $txtArr);
            if (count($txtArr[0]) > 0) {
                Debug::debug('Found: ' . implode("\n", $txtArr[1]));
                foreach ($txtArr[1] as $textTo) {
                    array_push($textToTranslate, $textTo);
                }
            }
            unset($txtArr);
            //label
            preg_match_all("/<label>(.*?)<\/label>/", $line, $txtArr);
            if (count($txtArr[0])) {
                Debug::debug('Found: ' . implode("\n", $txtArr[1]));
                foreach ($txtArr[1] as $textArr) {
                    array_push($textToTranslate, $textArr);
                }
            }
            //comment
            preg_match_all("/<comment>(.*?)<\/comment>/", $line, $txtArr);
            if (count($txtArr[0])) {
                Debug::debug('Found: ' . implode("\n", $txtArr[1]));
                foreach ($txtArr[1] as $textArr) {
                    array_push($textToTranslate, $textArr);
                }
            }
            //tooltip
            preg_match_all("/<tooltip>(.*?)<\/tooltip>/", $line, $txtArr);
            if (count($txtArr[0])) {
                foreach ($txtArr[1] as $textArr) {
                    array_push($textToTranslate, $textArr);
                }
            }
        }
        Debug::debug('--------------------------------------------------------');
        return $textToTranslate;
    }

}

